#classe de gestion des utilisateurs

from db_functions import connectdb
from password_hash import hash_password


class User:
    def __init__(self):
        __instance = None

    #fonction pour créer un utilisateur
    @staticmethod
    def adduser(username, password, phone):
        """
        :param username:
        :param password:
        :param phone:
        """
        hs_pwd = hash_password(password)

        db = connectdb()

        cursor = db.cursor()
        cursor_v = db.cursor()

        request_v = "SELECT COUNT(username) FROM md_department WHERE md_department.username = %s"
        request = "INSERT INTO md_department (username, password, phone) VALUES (%s, %s, %s)"

        values = (username, hs_pwd, phone)
        print(username)
        cursor_v.execute(request_v, (username,))
        data = cursor_v.fetchall()
        print(data[0])
        if data[0] != (0,):
            print("user already exist")
        else:
            cursor.execute(request, values)
            db.commit()
            print("record insterted")

    #fonction pour créer un administrateur
    @staticmethod
    def addadmin(username, password, site_admin, master_admin):
        """
        :param username:
        :param password:
        :param site_admin:
        :param master_admin:
        """
        hs_pwd = hash_password(password)

        db = connectdb()

        cursor = db.cursor()

        request = "INSERT INTO admin (username, password, site_admin, master_admin) VALUES (%s, %s, %s, %s)"

        values = (username, hs_pwd, site_admin, master_admin)

        cursor.execute(request, values)

        db.commit()

        print("record insterted")

    #fonction pour sélectionner un utilisateur en db en fonction de son username: return l'id
    @staticmethod
    def selectuser(username):
        """
        :param username:
        :return:
        """
        db = connectdb()

        cursor = db.cursor()

        request = "SELECT * FROM md_department WHERE md_department.username = %s"

        cursor.execute(request, (username,))

        data = cursor.fetchone()

        return data[0]

    @staticmethod
    def deleteuser(username):
        """
        :param username:
        """
        db = connectdb()

        cursor = db.cursor()

        request = "DELETE FROM md_department WHERE md_department.username = %s "

        cursor.execute(request, (username,))

        print("delete sucessfull")

        db.commit()

    #fonction pour séléctionner un admin en db
    @staticmethod
    def selectadmin(username):
        """
        :param username:
        :return:
        """
        db = connectdb()

        cursor = db.cursor()

        request = "SELECT * FROM admin WHERE admin.username = %s"

        cursor.execute(request, (username,))

        data = cursor.fetchone()

        return data[0]

    #fonction pour supprimer un admin
    @staticmethod
    def deleteadmin(id_admin):
        """
        :param id_admin:
        """
        db = connectdb()

        cursor = db.cursor()

        request = "DELETE FROM admin WHERE admin.id_admin = %s "

        cursor.execute(request, (id_admin,))

        print("delete sucessfull")

        db.commit()


    #fonction de login utilisateur
    def login(self, username, password):
        """

        :param username:
        :param password:
        :return:
        """
        db = connectdb()

        password = hash_password(password)

        cursor = db.cursor()

        request = "SELECT password FROM md_department WHERE md_department.username = %s"

        cursor.execute(request, (username,))

        data = cursor.fetchone()

        if data[0] != password:
            print("incorrect password")
            return 0
        elif data[0] == password:
            print("logged")
            return 1
        else:
            return 0

    #fonction pour lister les utilisateurs
    @staticmethod
    def listusers():
        db = connectdb()

        cursor = db.cursor()

        request = "SELECT username FROM md_department"

        cursor.execute(request)

        data = cursor.fetchall()

        users = []

        for row in data:
           users.append(row[0])

        return users

    #fonction de login administrateur
    def loginadmin(self, username, password):
        """
        :param username:
        :param password:
        :return:
        """
        db = connectdb()

        password = hash_password(password)
        cursor = db.cursor()

        request = "SELECT password FROM admin WHERE admin.username = %s"

        cursor.execute(request, (username,))

        data = cursor.fetchone()

        if data[0] != password:
            print("incorrect password")
            return 0
        elif data[0] == password:
            print("logged")
            return 1
        else:
            return 0

