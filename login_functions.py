#Fonctionnalité mise en place pour un groupe de 4
#double authentification par sms

from db_functions import connectdb
from random import *
import requests

#fonction pour envoyer le token 2FA par sms
def sendtoken(username, token):
    db = connectdb()

    cursor = db.cursor()

    request = "SELECT phone FROM md_department WHERE md_department.username = %s "

    cursor.execute(request, (username,))

    data = cursor.fetchone()

    print(type(data[0]))

    url = "https://api.mailjet.com/v4/sms-send"

    payload = "{\r\n\"From\": \"Clic Med\",\r\n \"To\": \""+str(data[0])+"\",\r\n \"Text\": \"Hello your token for access to clic med system: "+ str(token)+"\"\r\n}"
    headers = {
        'Authorization': "Bearer 343395635c6a447687753185875d8fff",
        'Content-Type': "application/json",
        'User-Agent': "PostmanRuntime/7.13.0",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Postman-Token': "63a98523-f646-47ef-955f-2652bbb3b342,20cfd2f2-7ba3-4a95-ad99-52e0defae61e",
        'Host': "api.mailjet.com",
        'accept-encoding': "gzip, deflate",
        'content-length': "74",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)

#fonction de génération du token
def generatetoken(username):
    token = randint(10000,99999)
    db = connectdb()

    cursor = db.cursor()
    request = "UPDATE md_department SET token = %s WHERE md_department.username = %s"

    values = (token, username)

    cursor.execute(request, values)

    sendtoken(username, token)


#fonction de vérification du token
def verifytoken(tokensais, username):
    db = connectdb()

    cursor = db.cursor()
    request = "SELECT token FROM md_department WHERE md_department.username = %s"

    cursor.execute(request, (username,))

    data = cursor.fetchone()

    if data[0] != tokensais:
        return 0
    else:
        return 1


