from ftp_functions import ftpconnect
from sys import stdout
import os
import hashlib


class Ftpmgmt:
    def __init__(self, server, username, password):
        self.server = server
        self.username = username
        self.password = password
        self.ftp = ftpconnect(self.server, self.username, self.password)

    def listfiles(self):
        files = []
        self.ftp.retrlines('NLST' ,files.append)

        return files

    def uploadfile(self, fileurl):
        fp = open(fileurl, 'rb')
        self.ftp.storbinary('STOR %s' % os.path.basename(fileurl), fp, 1024)
        print("up done")
        fp.close()

    def delfile(self, filename):
        self.ftp.delete(filename)

    def renfile(self, curr_filename, new_filename):
        self.ftp.rename(curr_filename, new_filename)

    def mkdir(self, dirname):
        self.ftp.voidcmd("MKD " + dirname)

    def rmdir(self, dirname):
        self.ftp.rmd(dirname)

    def dlfile(self ,filein, fileout):
        fo = open(fileout, 'wb')
        self.ftp.retrbinary("RETR " + filein, fo.write)
        fo.close()

    def retrdir(self):
        self.ftp.dir()


