from tkinter import *
from tkinter.filedialog import *
from ftp_managment import *
from user_managment import *
from tkinter.messagebox import *
from backgroudfiletask import *
from login_functions import *


class Application:
    def __init__(self, username, password):
        self.ffp = Ftpmgmt("127.0.0.1", username, password)
        self.root = Tk()
        self.root.title("ClicMed FTP")
        self.listfile()

        self.upbutton = Button(self.root, text="Upload", command=lambda :Popupfile(username, password))
        self.upbutton.grid(column=1, row=2)

        self.delbutton = Button(self.root, text="Delete", command=lambda :self.delfileint())
        self.delbutton.grid(column=2, row=2)

        self.refreshbut = Button(self.root, text="Refresh", command=lambda: self.listfile())
        self.refreshbut.grid(column=3, row=2)

        self.saveasbut = Button(self.root, text="Download", command=lambda: self.downloadfileint())
        self.saveasbut.grid(column=4, row=2)

        self.mkdirbut = Button(self.root, text="Add Dir", command=lambda: Popupfolder(username, password))
        self.mkdirbut.grid(column=5, row=2)

        self.root.mainloop()

    def listfile(self):
       self.list = Listbox(self.root)
       self.files = self.ffp.listfiles()
       for x in self.files:
           self.list.insert(END, x)
       self.list.grid(row=1, column=1)

    def uploadfile(self):
        self.filepath = askopenfilename(title="Import File", filetypes=[('all files','.*')])
        self.ffp.uploadfile(self.filepath)
        self.listfile()

    def delfileint(self):
        self.value = str((self.list.get(ACTIVE)))
        print(self.value)
        self.ffp.delfile(self.value)
        self.listfile()

    def downloadfileint(self):
        self.value = str((self.list.get(ACTIVE)))
        self.file = asksaveasfile(initialfile = self.value ,mode='w')
        self.ffp.dlfile(self.value, self.file.name)


class Popupfolder(Application):
    def __init__(self, username, password):

        self.ffp = Ftpmgmt('127.0.0.1', username, password)
        self.rootfold = Tk()

        self.rootfold.title("Add Folder")

        self.dirname = Label(self.rootfold, text="username")
        self.dirname.grid(column=1, row=1)

        self.dirnameval = StringVar(master=self.rootfold)
        self.dirnameentry = Entry(self.rootfold, textvariable=self.dirnameval, width=30)
        self.dirnameentry.grid(column=2, row=1)

        self.dirvalid = Button(self.rootfold, text="Login", command=lambda: self.crfld())
        self.dirvalid.grid(column=1, row=3)

    def crfld(self ):
        self.valuename = self.dirnameval.get()

        self.ffp.mkdir(self.valuename)

        self.rootfold.destroy()


class Popupfile(Application):
    def __init__(self, username, password):
        self.ffp = Ftpmgmt('127.0.0.1', username, password)
        self.rootfile = Tk()
        self.rootfile.title("Upload File")

        self.patientcode = Label(self.rootfile, text="Patient Code")
        self.patientcode.grid(column=1, row=1)

        self.patientcodeval = StringVar()
        self.patientcodefield = Entry(self.rootfile, textvariable=self.patientcodeval, width=30)
        self.patientcodefield.grid(column=2, row=1)

        self.version = Label(self.rootfile, text="Version Number")
        self.version.grid(column=1, row=2)

        self.versionval = StringVar()
        self.versionfield = Entry(self.rootfile, textvariable=self.versionval)
        self.versionfield.grid(column=2, row=2)

        self.upbutton = Button(self.rootfile, text="Upload File", command=lambda: self.uploadfilepop())
        self.upbutton.grid(column=1, row=3)

    def uploadfilepop(self):
        self.filepath = askopenfilename(title="Import File", filetypes=[('all files', '.*')])
        directory = os.path.split(self.filepath)[0]
        print(self.filepath)
        print(md5check(directory))
        self.ffp.uploadfile(self.filepath)
        self.rootfile.destroy()



class Login:
    def __init__(self):
        self.login = Tk()
        self.login.title("Login ClidMed")

        self.usr = User()

        self.usernamelab = Label(self.login, text="username")
        self.usernamelab.grid(column=1, row=1)

        self.loginval = StringVar()
        self.loginentry = Entry(self.login, textvariable=self.loginval, width=30)
        self.loginentry.grid(column=2, row=1)

        self.passwordlab = Label(self.login, text="password")
        self.passwordlab.grid(column=1, row=2)

        self.passwordval = StringVar()
        self.passwordentry = Entry(self.login, textvariable=self.passwordval, show="*", width=30)
        self.passwordentry.grid(column=2, row=2)

        self.loginbut = Button(self.login, text="Login", command=lambda: self.loginflow())
        self.loginbut.grid(column=1, row=3)

        self.login.mainloop()

    def printest(self, password):
        print(password)


    def loginflow(self):
        self.loginvalue = self.loginval.get()
        self.passwordvalue = self.passwordval.get()
        if(self.usr.login(self.loginvalue, self.passwordvalue) == 1):
            Token(self.loginvalue, self.passwordvalue)

        else:
            self.error = showerror("error", "incorrect login")


class Token():
    def __init__(self, loginvalue, password):

        self.login = loginvalue
        self.password = password

        self.tokenwindows = Tk()
        self.tokenwindows.title("Token Access")

        generatetoken(self.login)

        self.labtok = Label(self.tokenwindows, text="Your Token")
        self.labtok.grid(column=1, row=1)

        self.tokenval = StringVar(master=self.tokenwindows)
        self.token = Entry(self.tokenwindows, textvariable=self.tokenval, width=30)
        self.token.grid(column=2, row=1)

        self.validbut = Button(self.tokenwindows, text="valid", command= lambda : self.validtoken())
        self.validbut.grid(column=1, row=2)


        self.tokenwindows.mainloop()

    def validtoken(self):
        token = self.tokenval.get()
        print(token)

        if verifytoken(token, self.login) ==  1:
            Application(self.login, self.password)
        else:
            self.error = showerror("error", "incorrect Token")