import hashlib

def hash_password(input_password):
    hashed_pwd = hashlib.md5(input_password.encode())
    return hashed_pwd.hexdigest()

