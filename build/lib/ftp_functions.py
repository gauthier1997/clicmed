from ftplib import FTP


def ftpconnect(server, username, password):

    try:
        ftp = FTP(server, timeout=3600)
        try:
            ftp.login(user=username, passwd=password)
            print(ftp.getwelcome())
            return ftp
        except ftp.all_errors as err:
            print("LOGIN ERROR: ", err)

    except ftp.all_errors as error:
        print("SERVER ERROR: ", error)


