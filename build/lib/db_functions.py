import mysql.connector


def connectdb():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="",
        database="clicmed"
    )

    return mydb
