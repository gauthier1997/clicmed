#application coté administration

from tkinter.filedialog import *
from tkinter.messagebox import *
from user_managment import *

#classe fenetre principale
class Adminapp():
    def __init__(self):
        self.usr = User()
        self.root = Tk()
        self.root.title('ClicMed Admin')
        self.listuser()

        self.adduserbut = Button(self.root, text="Add User", command= lambda: Adduserpop())
        self.adduserbut.grid(column=1, row=2)

        self.addadminbut = Button(self.root, text="Add Admin", command=lambda: Addadminpop())
        self.addadminbut.grid(column=2, row=2)

        self.delbut = Button(self.root, text="Delete User", command= lambda : self.deleteuser())
        self.delbut.grid(column=3, row=2)


        self.root.mainloop()

    #fonction de list user et affichage dans la fenetre
    def listuser(self):
       self.list = Listbox(self.root)
       self.user = self.usr.listusers()
       for x in self.user:
           self.list.insert(END, x)
       self.list.grid(row=1, column=1)

    #fonction de delete user
    def deleteuser(self):
        print(self.list.get(ACTIVE))
        self.usr.deleteuser(self.list.get(ACTIVE))
        self.listuser()

#fenetre d'ajout utilisateur
class Adduserpop():
    def __init__(self):
        self.addwindow = Tk()
        self.addwindow.title("Adduser")
        self.usr = User()

        self.usernamelab = Label(self.addwindow, text='Username')
        self.usernamelab.grid(column=1, row=1)

        self.usernameval = StringVar(master=self.addwindow)
        self.username = Entry(self.addwindow, textvariable=self.usernameval, width=30)
        self.username.grid(column=2, row=1)

        self.passwordlab = Label(self.addwindow, text="Password")
        self.passwordlab.grid(column=1, row=2)

        self.passwordval = StringVar(master=self.addwindow)
        self.password = Entry(self.addwindow, textvariable=self.passwordval, width=30)
        self.password.grid(column=2, row=2)

        self.phonelab = Label(self.addwindow, text="Phone")
        self.phonelab.grid(column=1, row=3)

        self.phoneval = StringVar(master=self.addwindow)
        self.phone = Entry(self.addwindow, textvariable=self.phoneval, width=30)
        self.phone.grid(column=2, row=3)

        self.validbut = Button(self.addwindow, text="valid", command=lambda: self.getvar())
        self.validbut.grid(column=1, row=4)

        self.addwindow.mainloop()

    def getvar(self):
        self.username = self.usernameval.get()
        self.password = self.passwordval.get()

        print(self.username)
        self.usr.adduser(self.username, self.password)

#fenetre d'ajout administrateur
class Addadminpop():
    def __init__(self):
        self.addadm = Tk()
        self.addadm.title("Add Administrator")
        self.usr = User()

        self.usernamelab = Label(self.addadm, text='Username')
        self.usernamelab.grid(column=1, row=1)

        self.usernameval = StringVar(master=self.addadm)
        self.username = Entry(self.addadm, textvariable=self.usernameval, width=30)
        self.username.grid(column=2, row=1)

        self.passwordlab = Label(self.addadm, text="Password")
        self.passwordlab.grid(column=1, row=2)

        self.passwordval = StringVar(master=self.addadm)
        self.password = Entry(self.addadm, textvariable=self.passwordval, width=30)
        self.password.grid(column=2, row=2)

        self.sitelab = Label(self.addadm, text="Site")
        self.sitelab.grid(column=1, row=3)

        self.siteval = StringVar(master=self.addadm)
        self.site = Entry(self.addadm, textvariable=self.siteval, width=30)
        self.site.grid(column=2, row=3)

        self.validbut = Button(self.addadm, text="valid", command=lambda: self.getvar())
        self.validbut.grid(column=1, row=4)

    def getvar(self):
        self.usrnm = self.usernameval.get()
        self.passwd = self.passwordval.get()
        self.stadm = self.siteval.get()
        self.usr.addadmin(self.usrnm, self.passwd, self.stadm, 0)



        self.addadm.mainloop()

#fenetre de login
class Login():
    def __init__(self):
        self.rootlog = Tk()
        self.rootlog.title("Admin Login ClicMed")

        self.usr = User()

        self.usernamelab = Label(self.rootlog, text="username")
        self.usernamelab.grid(column=1, row=1)

        self.loginval = StringVar()
        self.loginentry = Entry(self.rootlog, textvariable=self.loginval, width=30)
        self.loginentry.grid(column=2, row=1)

        self.passwordlab = Label(self.rootlog, text="password")
        self.passwordlab.grid(column=1, row=2)

        self.passwordval = StringVar()
        self.passwordentry = Entry(self.rootlog, textvariable=self.passwordval, show="*", width=30)
        self.passwordentry.grid(column=2, row=2)

        self.loginbut = Button(self.rootlog, text="Login", command=lambda: self.loginflow())
        self.loginbut.grid(column=1, row=3)


        self.rootlog.mainloop()
    #fonction de procédure de login
    def loginflow(self):
        self.loginvalue = self.loginval.get()
        self.passwordvalue = self.passwordval.get()
        if (self.usr.loginadmin(self.loginvalue, self.passwordvalue) == 1):
            self.app = Adminapp()

        else:
            self.error = showerror("error", "incorrect login")