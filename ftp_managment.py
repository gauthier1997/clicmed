#module de managment du ftp
from ftp_functions import ftpconnect
from sys import stdout
import os
import hashlib


class Ftpmgmt:
    #intansiation de la classe
    def __init__(self, server, username, password):
        self.server = server
        self.username = username
        self.password = password
        self.ftp = ftpconnect(self.server, self.username, self.password)

    #fonction pour lister les fichier présent
    def listfiles(self):
        files = []
        self.ftp.retrlines('NLST' ,files.append)

        return files

    #fonction pour uploader un fichier
    def uploadfile(self, fileurl):
        fp = open(fileurl, 'rb')
        self.ftp.storbinary('STOR %s' % os.path.basename(fileurl), fp, 1024)
        print("up done")
        fp.close()

    #fonction pour supprimer un fichier
    def delfile(self, filename):
        self.ftp.delete(filename)

    #fonction pour renommer un fichier (non utiliser dans le programme, mais fait parti du CRUD Create Read Update Delete)
    def renfile(self, curr_filename, new_filename):
        self.ftp.rename(curr_filename, new_filename)

    #fonction creation d'un répertoire
    def mkdir(self, dirname):
        self.ftp.voidcmd("MKD " + dirname)

    #fonction supression réptertoire
    def rmdir(self, dirname):
        self.ftp.rmd(dirname)

    #fonction télécharment d'un fichier depuis le ftp
    def dlfile(self ,filein, fileout):
        fo = open(fileout, 'wb')
        self.ftp.retrbinary("RETR " + filein, fo.write)
        fo.close()

    def retrdir(self):
        self.ftp.dir()


